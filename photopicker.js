let inputPhoto = document.querySelector("#photopicker");
let btnphoto = document.querySelector("#btn-photo");
let photomodal = document.querySelector("#photo-modal");
let target = "#myModal"

let urlapi = inputPhoto.dataset.urlapi;
let urlphoto = inputPhoto.dataset.urlphoto;

let modal = `
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Capturar Foto</h4>
                </div>
                <div class="modal-body">
                    <div id="fotoCapturada" class="thumbnail">
                        <img id="imagemConvertida" class="img-responsive" />
                    </div>
                    <div id="ca-camera" class="embed-responsive embed-responsive-4by3">

                        <video class="img-responsive" autoplay="true" id="webCamera">
                        </video>

                    </div>

                    <input type="hidden" id="base_img" name="base_img" />

                </div>
                <div class="modal-footer">
                    <button id="take-photo" type="button" class="btn btn-primary">
                        <span class="glyphicon glyphicon-camera" aria-hidden="true"></span> Tirar foto
                    </button>
                    <button id="new-photo" type="button" class="btn btn-warning">
                        <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Nova foto
                    </button>
                    <button id="btn-photo-close" type="button" class="btn btn-success" data-dismiss="modal">
                        <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Inserir
                    </button>
                </div>
            </div>
        </div>
    </div>
`;

photomodal.innerHTML = modal;

function loadCamera() {
    $("#fotoCapturada").hide();
    //Captura elemento de vídeo
    let video = document.querySelector("#webCamera");
    $("#ca-camera").show();

    //Verifica se o navegador pode capturar mídia
    if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({
            audio: false,
            video: {
                facingMode: 'user'
            }
        })
            .then(function (stream) {
                //Definir o elemento vídeo a carregar o capturado pela webcam
                video.srcObject = stream;
            })
            .catch(function (error) {
                alert("Oooopps... Falhou :'(");
            });
    }
}

function takeSnapShot() {
    //Captura elemento de vídeo
    let video = document.querySelector("#webCamera");

    //Criando um canvas que vai guardar a imagem temporariamente
    let canvas = document.createElement('canvas');
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    let ctx = canvas.getContext('2d');

    //Desenhando e convertendo as dimensões
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

    //Criando o JPG
    let dataURI = canvas.toDataURL('image/jpeg'); //O resultado é um BASE64 de uma imagem.
    document.querySelector("#base_img").value = dataURI;

    stopCamera(); //para de capturar os dados
    sendSnapShot(dataURI); //Gerar Imagem e Sallet Caminho no Banco
}

function stopCamera() {
    let video = document.querySelector("#webCamera");
    let track = video.srcObject.getVideoTracks()[0];
    track.stop();
    $("#ca-camera").hide();
}

function sendSnapShot(base64) {

    let fotoNome = $(inputPhoto).val();
    let request = new XMLHttpRequest();
    let req = '';

    request.open('POST', urlapi, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            //Colocar o caminho da imagem no SRC
            let data = JSON.parse(request.responseText);

            //verificar se houve erro
            if (data.error) {
                alert(data.error);
                return false;
            }

            //Mostrar informações
            $("#imagemConvertida").attr("src", urlphoto + '/' + data.img);
            $("#fotoCapturada").show();
            $(inputPhoto).val(data.img);
            $(inputPhoto).trigger("blur");
            $("#ca-msg-individual").hide();
        } else {
            alert("Erro ao sallet. Tipo:" + request.status);
        }
    };

    if (fotoNome === '') {
        req = "base_img=" + base64 + "&fotoAtual=VAZIO";
    } else {
        req = "base_img=" + base64 + "&fotoAtual=" + fotoNome;
    }

    request.onerror = function () {
        alert("Erro ao salvar. Back-End inacessível.");
    }

    request.send(req);
}

$(target).on('hidden.bs.modal', function (e) {
    stopCamera();
});

$(btnphoto).on("click", function(){
    $("#btn-photo-close").hide();
    $(inputPhoto.dataset.target).modal('show');
    $("#take-photo").show();
    $("#new-photo").hide();
    loadCamera();
});

$("#take-photo").on("click", function(e){
    takeSnapShot();
    $("#new-photo").show();
    $(this).hide();
    $("#btn-photo-close").show();

});

$("#new-photo").on("click", function(e){
    $("#new-photo").hide();
    $("#btn-photo-close").hide();
    loadCamera();
    $("#take-photo").show();
    $(this).hide();
});

$(inputPhoto).on("blur", function(e){
    //por icone de foto presente
});